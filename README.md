# Data Engineering collection

A mostly unstructured and random collection of interesting links and resources about data engineering. 

## Other lists

* https://github.com/gunnarmorling/awesome-opensource-data-engineering
* https://github.com/jghoman/awesome-apache-airflow
* https://github.com/pditommaso/awesome-pipeline
* https://github.com/veggiemonk/awesome-docker

## Frameworks / Tools

* [Marquez](https://marquezproject.github.io/marquez/): ETL pipeline process metadata schema
* [Airbyte](https://github.com/airbytehq/airbyte)
* [Kedro](https://github.com/quantumblacklabs/kedro)
* [Amundsen](https://amundsen.io): metadata catalog with support for automatic (?) data structure imports
* [Egeria](https://odpi.github.io/egeria-docs/): open metadata framework by the Linux Foundation
  * includes madly detailed [data lineage documentation](https://odpi.github.io/egeria-docs/features/lineage-management/overview/#lineage) based on Open Lineage specification

### Test data / data generators

* [datamaker](https://github.com/glynnbird/datamaker) CLI tool, ([web UI](https://glynnbird.github.io/datamakerui/))
* Python [testdata](https://github.com/arieb/python-testdata) library

## Python

* [Great Expectations](https://greatexpectations.io/): data quality testing/assurance framework
* [ETL with Airflow](https://gtoonstra.github.io/etl-with-airflow/#): collections/write-up of best practices

## (Scientific) papers

* [Lakehouse: A New Generation of Open Platforms that Unifiy Data Warehousing and Advanced Analytics](http://cidrdb.org/cidr2021/papers/cidr2021_paper17.pdf)

## Blog articles / Blogs

* [Proposal](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions) of _Architecture Decision Reports_ (ADR) by Michael Nygard
* Articles by Maxime Beauchemin published at Medium: https://maximebeauchemin.medium.com/
* [Seven stages of expertise in software engineering](http://wayland-informatics.com/The%20Seven%20Stages%20of%20Expertise%20in%20Software.htm)
* [Data & AI landscape 2021](https://mattturck.com/data2021/) by Matt Turk

## Podcasts

* [Data Engineering Podcast](https://www.dataengineeringpodcast.com/)

# Copyright and License

Copyright (C) 2021 Markus Suhr

All content of this repository is committed to the public domain, [CC0 license](LICENSE). Linked resources are licensed individually and are not in any way tied to this collection.